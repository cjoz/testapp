package testapp;



import testapp.parsers.SeleniumScriptParser;
import testapp.testing.SeleniumTest;
import testapp.testing.TestExecutor;

public class TestApp {

    // название файла с тестом записывается в 1-й параметр командной строки
    public static void main(String[] args) {
        if (args.length>0) {
            String filename = args[0];
            String fileExt = filename.lastIndexOf('.') >= 0 ? filename.substring(filename.lastIndexOf('.') + 1) : "";
            // запись пути к драйверу браузера
            System.setProperty("webdriver.chrome.driver", "driver\\chromedriver.exe");
            try {
                SeleniumTest seleniumTest = SeleniumScriptParser.createParser(fileExt).parseFile(filename);
                new TestExecutor().execute(seleniumTest);
            } catch (ClassNotFoundException ex) {
                System.err.println("Формат файла " + fileExt + " не поддерживается.");
            } catch (Exception ex) {
                System.err.println("Произошла ошибка при чтении файла.");
                ex.printStackTrace();
            }
        }
        else System.out.println("Использование: TestApp filename\n* filename: название файла с тестом");
    }
}
