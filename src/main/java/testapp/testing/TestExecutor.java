package testapp.testing;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Date;

// класс, определяющий методы, которые реализуют шаги теста
public class TestExecutor {

    private WebDriver driver;

    public TestExecutor() {
        driver = new ChromeDriver();
    }
    public TestExecutor(WebDriver driver) {
        this.driver = driver;
    }


    public void openurl(String param) {
        driver.get(param);
    }
    public void click(String param) {
        // в случае использования ajax на сайте (например, после ввода данных) нужный WebElement появится не сразу
        WebDriverWait wait1 = new WebDriverWait(driver, 10);
        // XPath или селектор CSS
        By paramType = param.startsWith("/")||param.startsWith("(")? By.xpath(param) : By.cssSelector(param);
        WebElement element1 = wait1.until(ExpectedConditions.elementToBeClickable(paramType));
        element1.click();
    }
    public void setvalue(String param) {
        int delimiterIndex = param.indexOf('|');
        String xpath = param.substring(0,delimiterIndex).trim();
        String value = param.substring(delimiterIndex+1).trim();
        findElementByParam(xpath).sendKeys(value);
    }
    public void checkelementvisible(String param) {
        // наличие и видимость проверяется в момент запроса
        try {
            String isHidden = findElementByParam(param).getAttribute("hidden");
            if (isHidden == null)
                System.out.println("Элемент отображается");
            else System.out.println("Элемент скрыт");
        }
        catch (NoSuchElementException ex) {
            System.out.println("Элемент отсутствует");
        }

    }
    public void screenshot(String param) {
        TakesScreenshot capturer = (TakesScreenshot) driver;
        File screenshot = capturer.getScreenshotAs(OutputType.FILE);
        // папка, куда сохраняются скринщоты
        File scrLocation = new File("test\\screens\\");
        scrLocation.mkdirs();
        File a = new File(scrLocation, new Date().toString().replace(" ", "_").replace(":","-")+".png");
        screenshot.renameTo(a);
    }

    // поиск элемента по XPath или по селектору CSS
    // XPath начинается со слэша
    WebElement findElementByParam(String param) {
        // xpath без начального слеша делает его неопределенным для поиска в целом документе
        if (param.startsWith("/") || param.startsWith("("))
            return driver.findElement(By.xpath(param));
        else return driver.findElement(By.cssSelector(param));
    }

    // Выполняет действия, заданные тестом, по порядку
    // неподдерживаемые действия пропускаются
    // Формат метода, реализующего действие: actionnameinlowercase(String)
    // Рефлективный метод избавляет от надобности использовать if/switch для поиска действия
    public  void execute(SeleniumTest test) {
        Class<? extends TestExecutor> thisClass = getClass();
        for (int i =0; i<test.actionCount(); i++) {
            String actionName = test.getActionName(i).toLowerCase();
            try {
                Method actionHandler = thisClass.getMethod(actionName, test.getParam(0).getClass());
                actionHandler.invoke(this, test.getParam(i));
            }
            catch (NoSuchMethodException ex) {
                System.err.println("Действие "+test.getActionName(i)+" не поддерживается, "+(i+1)+"-й этап теста пропущен.");
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }


}
