package testapp.testing;

// тест, который необходимо выполнить
public class SeleniumTest {

    public static class Action {
        // действие
        public String action;
        // параметр
        public String param;
        public Action(String a, String p) {
            action=a;
            param=p;
        }
    }
    private String[] actions;
    private String[] params;
    public SeleniumTest(String[] actions, String[] params) {
        this.actions = actions;
        this.params = params;
    }

    public String[] getActionNames() {
        return actions;
    }

    public String[] getParams() {
        return params;
    }

    public String getActionName(int i) {
        return actions[i];
    }

    public String getParam(int i) {
        return params[i];
    }

    public Action getAction(int i) {
        return new Action(actions[i],params[i]);
    }

    // число действий
    public int actionCount() {
        return actions.length;
    }
}
