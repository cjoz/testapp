package testapp.parsers;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import testapp.testing.SeleniumTest;

import java.io.File;
import java.io.IOException;

// Построение теста из JSON-файла c использованием библиотеки Jackson
// Файл определяет массив шагов теста; шаг может состоять из трех элементов
public class JsonScriptParser implements SeleniumScriptParser {
    // десериализуемый объект
    static class Action {
        String action;
        String params;
        String description;
        Action() {}
        Action(String action, String params) {
            this.action=action;
            this.params=params;
        }
        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public String getParams() {
            return params;
        }

        public void setParams(String params) {
            this.params = params;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
    public SeleniumTest.Action formatAction(Action action) {
        return new SeleniumTest.Action(action.action, action.params);
    }
    @Override
    public SeleniumTest parseFile(String scriptFilename) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Action[] testActions = mapper.readValue(new File(scriptFilename), Action[].class);
        String[] actionValues = new String[testActions.length];
        String[] paramValues = new String[testActions.length];
        for (int i =0; i<testActions.length; i++) {
            actionValues[i] = testActions[i].action;
            paramValues[i] = testActions[i].params;
        }
        return new SeleniumTest(actionValues,paramValues);
    }
}
