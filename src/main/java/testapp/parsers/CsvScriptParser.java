package testapp.parsers;

import testapp.testing.SeleniumTest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

// Построение теста из CSV-файла
public class CsvScriptParser implements SeleniumScriptParser{

    @Override
    public SeleniumTest parseFile(String scriptFilename) throws IOException {
        List<String> csvStrings;
        String[] actions,params;
            int i;
            csvStrings = Files.readAllLines(Paths.get(scriptFilename));
            int actionColumn = -1, paramsColumn = -1;
            String[] headers = csvStrings.get(0).split(";");
            // хотя бы два столбца
            if (headers.length<2)
                throw new IOException("Заданный CSV-файл не представляет собой скрипт Selenium.");
            // проверка файла на заголовок
            for (i =0; i<headers.length; i++) {
                // установка столбцов с названием дейтвий и параметром
                if (headers[i].equalsIgnoreCase("actions"))
                    actionColumn = i;
                else if (headers[i].equalsIgnoreCase("params"))
                    paramsColumn = i;
            }
            // заголовка нет
            if (actionColumn==-1 || paramsColumn==-1) {
                actionColumn=0;
                paramsColumn=1;
                i=0;
            }
            else i = 1;
            actions = new String[csvStrings.size()-1];
            params = new String[csvStrings.size()-1];
            for (int k=0; i<csvStrings.size(); i++,k++) {
                String[] elements = csvStrings.get(i).split(";");
                actions[k] = elements[actionColumn];
                String rawParam = elements[paramsColumn];
                // строка может оказаться завернутой в кавычки
                if (rawParam.startsWith("\""))
                    rawParam = rawParam.substring(1,rawParam.length()-1);
                params[k] = rawParam.replace("\"\"", "\"");
            }
            return new SeleniumTest(actions,params);
    }
}
