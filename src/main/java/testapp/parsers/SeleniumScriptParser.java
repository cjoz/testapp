package testapp.parsers;

import testapp.testing.SeleniumTest;

import java.io.IOException;


public interface SeleniumScriptParser {
    SeleniumTest parseFile(String scriptFilename) throws IOException;

    // Создание экземпляра для объекта класса, реализующего SeleniumScriptParser
    // Название класса определяется в процессе выполнения исходя из типа файла, оно должно иметь вид [Type]ScriptParser
    // При добавлении класса метод исправлять не нужно
    static SeleniumScriptParser createParser(String fileType) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        String classNameExt = fileType.substring(0,1).toUpperCase().concat(fileType.substring(1).toLowerCase());
        Class parserClass = Class.forName("testapp.parsers."+classNameExt+"ScriptParser");
        return (SeleniumScriptParser)parserClass.newInstance();
    }
}
