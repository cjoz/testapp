package testapp.parsers;

import org.xml.sax.XMLReader;
import testapp.testing.SeleniumTest;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

// Построение теста из файла XML c использованием библиотеки Streaming API for XML
// элементы action и params должны размещаться в правильном порядке
public class XmlScriptParser implements SeleniumScriptParser{

    @Override
    public SeleniumTest parseFile(String scriptFilename) throws FileNotFoundException {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();

        try {
            List<String> actionValues = new ArrayList<>();
            List<String> paramValues = new ArrayList<>();
            InputStream in = new FileInputStream(scriptFilename);
            XMLEventReader xmlReader = inputFactory.createXMLEventReader(in);
            XMLEvent event = null;
            if (xmlReader.hasNext()) event = xmlReader.nextEvent();
            while (xmlReader.hasNext()) {
                if (event.isStartElement()) {
                    String valueType = event.asStartElement().getName().getLocalPart();
                    event = xmlReader.nextEvent();
                    if (event.isCharacters()) {
                        if (valueType.equalsIgnoreCase("action"))
                            actionValues.add(event.asCharacters().getData());
                        else if (valueType.equalsIgnoreCase("params")) {
                            // наличие некоторых неалфавитных символов может привести к тому, что текст одного элемента будет разделен
                            StringBuilder paramValue = new StringBuilder();
                            while (event.isCharacters()) {
                                paramValue.append(event.asCharacters().getData());
                                event = xmlReader.nextEvent();
                            }
                            paramValues.add(paramValue.toString());
                            // пропускаем переход к следующему элементу, он уже произошел в цикле while (event.isCharacters())
                            continue;
                        }
                    }
                    else if (event.isEndElement()) {
                        // пустой элемент
                        if (valueType.equalsIgnoreCase("action"))
                            actionValues.add("");
                        else if (valueType.equalsIgnoreCase("params"))
                            paramValues.add("");
                    }
                }
                event = xmlReader.nextEvent();
            }
            return new SeleniumTest(actionValues.toArray(new String[]{}), paramValues.toArray(new String[]{}));
        } catch (XMLStreamException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
